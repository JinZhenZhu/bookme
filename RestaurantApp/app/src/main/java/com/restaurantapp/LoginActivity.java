package com.restaurantapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by bwadmin on 29/11/17.
 */

public class LoginActivity extends Activity {
    private Button loginBtn;
    private EditText userName, password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        loginBtn=(Button)findViewById(R.id.login_btn);
        userName=(EditText) findViewById(R.id.username_login_layout);
        password=(EditText) findViewById(R.id.password_login_layout);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userName.getText().toString().equalsIgnoreCase("") || password.getText().toString().equalsIgnoreCase("")){
                    Toast.makeText(LoginActivity.this,"Please Enter Username and Password",Toast.LENGTH_SHORT).show();
                }else{
                    Intent in =new Intent(LoginActivity.this,RestaurantList_Activity.class);
                    startActivity(in);
                    finish();
                }

            }
        });
    }
}
