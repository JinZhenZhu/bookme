package com.restaurantapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ListView;

import com.restaurantapp.adapter.Restaurant_list_adapter;

/**
 * Created by bwadmin on 29/11/17.
 */

public class RestaurantList_Activity extends Activity {
    private ListView listview;
    private Restaurant_list_adapter adpter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restaurant_list_layout);
        adpter= new Restaurant_list_adapter(RestaurantList_Activity.this);
        listview= (ListView)findViewById(R.id.restaurant_list);
        listview.setAdapter(adpter);
    }
}
