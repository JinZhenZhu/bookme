package com.restaurantapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by bwadmin on 29/11/17.
 */

public class SignUpActivity extends Activity {
    private Button signUp;
    private EditText title, name, userName,password,phone, email;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);

        signUp=(Button)findViewById(R.id.signup_btn);
        title=(EditText) findViewById(R.id.title_signup_layout);
        name=(EditText) findViewById(R.id.name_signup_layout);
        userName=(EditText) findViewById(R.id.username_signup_layout);
        password=(EditText) findViewById(R.id.password_signup_layout);
        phone=(EditText) findViewById(R.id.phone_signup_layout);
        email=(EditText) findViewById(R.id.email_signup_layout);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(title.getText().toString().equalsIgnoreCase("") ||
                        name.getText().toString().equalsIgnoreCase("") ||
                        userName.getText().toString().equalsIgnoreCase("") ||
                        password.getText().toString().equalsIgnoreCase("") ||
                        phone.getText().toString().equalsIgnoreCase("") ||
                        email.getText().toString().equalsIgnoreCase(""))
                {
                    Toast.makeText(SignUpActivity.this,"Please Enter Valid Value",Toast.LENGTH_SHORT).show();
                }else{
                    Intent in =new Intent(SignUpActivity.this,LoginActivity.class);
                    startActivity(in);
                    finish();
                }

            }
        });
    }
}
