package com.restaurantapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;

import com.restaurantapp.R;
import com.restaurantapp.ReserveTable;

/**
 * Created by bwadmin on 29/11/17.
 */

public class Restaurant_list_adapter extends BaseAdapter {
    private Context context;
    private Button reserveTablebtn;
    private ImageView restaurant_img;
    int[] image_array = new int[] { R.drawable.res};

    public Restaurant_list_adapter(Context context){
       this.context=context;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.restaurant_items, parent, false);
            reserveTablebtn=(Button)convertView.findViewById(R.id.reserve_table_btn);
            restaurant_img = (ImageView)convertView.findViewById(R.id.restaurant_logo);

            //Picasso.with(context).load(image_array[0]).into(restaurant_img);

            reserveTablebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(context, ReserveTable.class);
                    context.startActivity(in);
                }
            });
        }
        return convertView;
    }

}
